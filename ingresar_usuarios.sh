#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  
agregar_usuario (){

echo "Escribe los nombres de los usuarios que debes ingresar..."
read user

if [ $user -eq $SUCCESS ];
	then
		echo "Usuario [$user] Este nombre ya existe..."
		exit 0
	else
		echo "Usuario [$user] Usuario creado... "
		
	fi
}

adduser $user

echo "Añade una contraseña para el usuario ..."
passwd $user

echo "Escriba el nombre del grupo al que se le va a añadir a este usuario ..."
read grupo

if [ $grupo -eq $SUCCESS ];
	then
		echo "Grupo -->[$grupo]<-- Presione enter para continuar ...."
	else
		echo "Grupo  -->[$grupo]<-- Este grupo no existe..."
		exit 0
	fi
}

sudo addgroup $user $grupo

echo "Informacion del usuario ..."
id $user

if [ $user -eq $SUCCESS ];
	then
		echo "Usuario [$user] Registrado..."
	else
		echo "Usuario [$user] No fue creado ... Error al crear el usuario..."
		exit 0
	fi
}
exit 0


