#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

#define MAX_STRING				100
#define MAX_TASK				100

	
#define ACTIVO 					1


typedef struct TASK Bitacora;

struct TASK{
 char titulo[MAX_STRING];
 char fecha[MAX_STRING];
 char desc[MAX_STRING];
};


Bitacora *crear_Task(char *_titulo, char *_fecha,char *_desc);
void agregar_Task(Bitacora *_array, Bitacora _t, int _index);
void ejecutar_Tasks(Bitacora *_array);



Bitacora *crear_Task(char *_titulo, char *_fecha,char *_desc){
Bitacora *_t = ( Bitacora *)malloc(sizeof( Bitacora));
	strcpy(_t->titulo, _titulo);
	strcpy(_t->fecha, _fecha);
	strcpy(_t->desc, _desc);
	return _t;

}
void agregar_Task(Bitacora *_array, Bitacora _t, int _index){
_array[_index] = _t;
}


void ejecutar_Tasks(Bitacora *_array){

for(int i = 0; i < MAX_TASK; i++){
if(_array[i].titulo == ACTIVO){
printf("\tLa tarea de nombre [%s], con Fecha [%s], tiene la caracteristica [%s] \n", _array[i].titulo, _array[i].fecha, _array[i].desc);
for(int j = 0; j < _array[i].fecha; j++){
sleep(1);
}
}
}
}

int main(int argc, char const *argv[]){
	 pthread_t hilo1;
         pthread_t hilo2;
         int a, b;
	  if( (a= pthread_create (&hilo1,NULL,Bitacora *t1 = crear_Task( "Tarea_1", "04 marzo 2020","Reproducir Música"))))
 {
      printf("la tarea : %d\n", a);
   }
  if( (b= pthread_create (&hilo2,NULL,Bitacora *t2 = crear_Task("Tarea_2",  "05 marzo 2020","Abrir Youtube"))))
 {
      printf("la tarea : %d\n", b);
   }

	 if( (a= pthread_create (&hilo2,NULL,Bitacora *t3 = crear_Task( "Tarea_3",  "06 marzo 2020","Escribir Texto en Word"))))
 {
      printf("la tarea : %d\n", a);
   }

	 if( (b= pthread_create (&hilo1,NULL,Bitacora *t4 = crear_Task( "Tarea_4",  "07 marzo 2020","Descargando Archivo"))))
 {
      printf("la tarea a fallado: %d\n", b);
   }
	

	Bitacora *array = (Bitacora*)malloc(sizeof(Bitacora)*MAX_TASK);

	pthread_create (&hilo1,NULL,agregar_Task(array, *t1, 0));
	pthread_create (&hilo1,NULL,agregar_Task(array, *t2, 1));
	pthread_create (&hilo1,NULL,agregar_Task(array, *t3, 2));
	pthread_create (&hilo1,NULL,agregar_Task(array, *t4, 3));

	pthread_create (&hilo1,ejecutar_Tasks(array));

   pthread_join( a, NULL);
   pthread_join( b, NULL); 

   exit(0);
  

}

	free(t1);
	free(t2);
	free(t3);
	free(t4);

	return 0;
}
#endif
