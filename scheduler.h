#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>



#define MAX_STRING				100
#define MAX_TASK				100

	
#define ACTIVO 					1


typedef struct TASK Bitacora;

struct TASK{
 char titulo[MAX_STRING];
 char fecha[MAX_STRING];
 char desc[MAX_STRING];
};


Bitacora *crear_Task(char *_titulo, char *_fecha,char *_desc);
void agregar_Task(Bitacora *_array, Bitacora _t, int _index);
void ejecutar_Tasks(Bitacora *_array);

#endif


