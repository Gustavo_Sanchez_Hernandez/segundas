#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Escriba el nombre del archivo con susrespectivos nombres de grupo que desea ingresar..."
   exit 1
fi
crearGrupo(){
	echo "----> Crear Grupo <----"
	eval nombreGrupo="$1"
	echo "nombreGrupo = ${nombreGrupo}"
	-p, --password PASSWORD
	groupadd "${nombreGrupo}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Grupo [${nombreGrupo}] agregado correctamente..."
	else
		echo "Grupo [${nombreGrupo}] No se pudo agregar..."
	fi
}

while IFS=: read -r f1
do
	crearGrupo "\${f1}"	
done < ${file}

exit 0
